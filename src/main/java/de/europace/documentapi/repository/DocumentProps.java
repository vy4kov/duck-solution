package de.europace.documentapi.repository;

import de.europace.documentapi.domain.Document;
import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "document")
@Value
public class DocumentProps {

    List<Document> props;

    public long getTotalCount() {
        return props.stream().count();
    }

    public long getDeletedCount() {
        return props.stream().filter(Document::getDeleted).count();
    }

    public long getTotalSize() {
        return props.stream().mapToLong(Document::getSize).sum();
    }

    public double getAverageSize() {
        return props.stream().mapToLong(Document::getSize).average().orElse(0.0);
    }

}
