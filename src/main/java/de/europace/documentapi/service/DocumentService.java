package de.europace.documentapi.service;

import de.europace.documentapi.domain.Document;
import de.europace.documentapi.repository.DocumentProps;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DocumentService {

    private static final String SORT_ERROR = "Invalid sorting. Allowed options: 'name', 'type', 'size' and 'deleted'.";

    private final DocumentProps documents;

    public List<Document> getDocuments(Optional<String> category, Optional<String> sort) {
        List<Document> result = applyFilter(category);

        if (sort.isPresent()) {
            switch (sort.get().toLowerCase()) {
                case "name":
                    return applySort(result, Comparator.comparing(Document::getName));
                case "type":
                    return applySort(result, Comparator.comparing(Document::getType));
                case "size":
                    return applySort(result, Comparator.comparing(Document::getSize));
                case "deleted":
                    return applySort(result, Comparator.comparing(Document::getDeleted));
                default:
                    throw new InvalidParameterException(SORT_ERROR);
            }
        }

        return result;
    }

    public long getTotalCount() {
        return documents.getTotalCount();
    }

    public long getDeletedCount() {
        return documents.getDeletedCount();
    }

    public long getTotalSize() {
        return documents.getTotalSize();
    }

    public double getAverageSize() {
        return documents.getAverageSize();
    }


    private List<Document> applyFilter(Optional<String> category) {
        return category.map(c -> documents.getProps().stream()
                .filter(document -> document.getCategories().contains(c))
                .collect(Collectors.toList())).orElseGet(documents::getProps);
    }

    private List<Document> applySort(List<Document> result, Comparator<Document> comparing) {
        return result.stream().sorted(comparing).collect(Collectors.toList());
    }
}

