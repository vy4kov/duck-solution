package de.europace.documentapi.api;

import de.europace.documentapi.domain.Document;
import de.europace.documentapi.service.DocumentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/v1/documents")
@AllArgsConstructor
public class DocumentController {

    private final DocumentService service;

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<Document>> getDocuments(
            @RequestParam(required = false) Optional<String> category,
            @RequestParam(required = false) Optional<String> sort) {
        try {
            return ResponseEntity.ok(service.getDocuments(category, sort));
        } catch (InvalidParameterException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/total-count")
    public ResponseEntity<Long> getTotalCount() {
        return ResponseEntity.ok(service.getTotalCount());
    }

    @GetMapping("/deleted-count")
    public ResponseEntity<Long> getDeletedCount() {
        return ResponseEntity.ok(service.getDeletedCount());
    }

    @GetMapping("/total-size")
    public ResponseEntity<Long> getTotalSize() {
        return ResponseEntity.ok(service.getTotalSize());
    }

    @GetMapping("average-size")
    public ResponseEntity<Double> getAverageSize() {
        return ResponseEntity.ok(service.getAverageSize());
    }

}
