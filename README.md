
### Instructions:
1. Open & Run project in IntelliJ
2. Use Postman or similar REST client to make GET requests:

### Basic Statistics:
localhost:8080/v1/documents/total-count

localhost:8080/v1/documents/deleted-count

localhost:8080/v1/documents/total-size

localhost:8080/v1/documents/average-size

### Sort By Name (Lexicographically Ascending)
localhost:8080/v1/documents/?sort=name

### Sort By Type (PDF >> IMG)
localhost:8080/v1/documents/?sort=type

### Sort By Size (0 >> 100)
localhost:8080/v1/documents/?sort=size

### Sort By Deleted (false >> true)
localhost:8080/v1/documents/?sort=deleted

### Error Handling: Returns Status 400 when invalid sort parameter.
localhost:8080/v1/documents/?sort=somethingelse
